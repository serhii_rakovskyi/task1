package com.example.serhii.test_1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int count = 0;

    final String LOG_TAG = "myLogs";
    SharedPreferences sPref;
    EditText ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView c = (TextView) findViewById(R.id.Count);
        loadCount();
        c.setText(String.valueOf(count) + " Counts");

        Button send = (Button)findViewById(R.id.Send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Send(ed.getText().toString());
            }
        });
        ed = (EditText) findViewById(R.id.editor);

    }


    private void saveCount() {
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt("Count", ++count);
        editor.commit();
    }

    private void loadCount() {
        sPref = getPreferences(MODE_PRIVATE);
        count = sPref.getInt("Count", 1);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveCount();
        Log.d(LOG_TAG, "onDestroy");
    }

    private void Send(String recipient){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        Log.d(LOG_TAG, recipient.toString());
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{recipient});
        startActivity(Intent.createChooser(i, "Send mail..."));
    }

}