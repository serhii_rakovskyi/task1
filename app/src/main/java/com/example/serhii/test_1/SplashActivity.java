package com.example.serhii.test_1;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private int SPLASH_TIME = 1000;
    private SplashActivity t = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(SPLASH_TIME);
                } catch (Exception e) {
                    e.printStackTrace();

                }
                Intent intent = new Intent(t, MainActivity.class);
                startActivity(intent);
                finish();
            }

        };
        timer.start();

    }
}
